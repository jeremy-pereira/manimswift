//
//  AppDelegate.swift
//  ManimSwift
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate
{



	func applicationDidFinishLaunching(_ aNotification: Notification)
	{
		// Insert code here to initialize your application
	}

	func applicationWillTerminate(_ aNotification: Notification)
	{
		// Insert code here to tear down your application
	}


}

