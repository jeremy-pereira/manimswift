//
//  SquareToCircle.swift
//  ManimSwift
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import ManimLib
/*
class SquareToCircle(Scene):
def construct(self):
circle = Circle()
square = Square()
square.flip(RIGHT)
square.rotate(-3 * TAU / 8)
circle.set_fill(PINK, opacity=0.5)

self.play(ShowCreation(square))
self.play(Transform(square, circle))
self.play(FadeOut(square))
*/

class SquareToCircle: Scene
{
	public override init(config: Config) throws
	{
		try super.init(config: config)
	}
	
	override func construct() throws
	{
		let circle = Circle()
		let square = Square()
		square.flip(axis: .right)
		square.rotate(radians: -3 * Double.tau / 8)
		circle.setFill(colour: .pink, opacity: 0.8)

		try self.play([ShowCreation(square)])
		try self.play([Transform(square, target: circle)])
		try self.play([FadeOut(square)])
	}
}

