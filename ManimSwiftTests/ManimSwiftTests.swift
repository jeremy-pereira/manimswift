//
//  ManimSwiftTests.swift
//  ManimSwiftTests
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import ManimSwift
import ManimLib

class ManimSwiftTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


    /// This is basically just a hook so we can start coding without a user
	/// interface. The sdquare to circle example is a direct translation of the
	/// Manim one.
    func testSquareToCircle()
	{
		let config = Config()
		do
		{
			let stoc = try SquareToCircle(config: config)
			try stoc.construct()
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

}
