//
//  Config.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 06/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Replaces configuration functionality that was in Container.
///
/// In Manim the `Container` class also provided the configuration functionality.
/// We separate it here.
public protocol ConfigurableObject
{
	/// The configuration for this object
	var config: Config { get }
}

/// Holds configuration parameters
///
/// Manages the possible default values. Ultimately, this is backed by
/// `UserDefaults` so configuration can be set manually  or controlled by the
/// defaults system.
///
/// Note that there is a layer of "local configuration" that takes higher
/// precedence than the user defaults. This allows us to have per document
/// configuration that doesn't clash if we have multiple documents open at
/// once.
///
/// Since it is backed by `UserDefaults` values must conform to the rules for
/// property list values.
/// - todo: Have a mechnism to store any codeable value.
public struct Config
{
	private var localValues: [Key : Any]

	public init(_ values: [Key : Any] = [:])
	{
		localValues = values
	}


	/// Add new values to the configuration.
	///
	/// The values added are local values meaning that the `UserDefaults` object
	/// is not changed.
	///
	/// - Parameters:
	///   - newValues: New values to add to the configuration
	///   - shouldReplace: If true, we will replace the values for existing
	///                    keys. If false and a key exists, the new value is
	///                    discarded. Defaults to `false`
	/// - Returns: A new config set appropriately.
	public func adding(_ newValues: [Key : Any], override shouldReplace: Bool = false) -> Config
	{
		var combinedValues = localValues
		for (key, value) in newValues
		{
			if self[key] == nil  || shouldReplace
			{
				combinedValues[key] = value
			}
		}
		return Config(combinedValues)
	}

	public subscript(key: Key) -> Any?
	{
		if let ret = localValues[key]
		{
			return ret
		}
		else
		{
			guard let ret = UserDefaults.standard.object(forKey: key.rawValue)
				else { return nil }
			return ret
		}
	}
}

extension Config
{
	/// The default width of lines used to sroke shapes
	public static let defaultStrokeWidth: Double = 4
}

extension Config
{

	/// An enumeration for describing keys in the config file.
	///
	/// We could use `String` as the key but this is more type safe.
	public enum Key: String
	{
		case alwaysUpdateMObjects
		case anchorsSpanFullRange
		/// Centre of an arc
		case arcCentre
		case backgroundColour
		case backgroundImage
		case backgroundOpacity
		case backgroundStrokeColour
		case backgroundStrokeOpacity
		case backgroundStrokeWidth
		case cairoLineWidthMultiple
		case cameraClass
		case cameraConfig
		case closeNewPoints
		case colour
		case dim
		case endAtAnimationNumber
		/// A directory name specifying where to put the scene files under the
		/// output directory.
		///
		/// Defaults to the class name of the scene.
		case fileName
		case fillColour
		case fillOpacity
		case frameCentre
		case frameHeight
		case frameWidth
		case gifFileExtension
		case imageMode
		case lagRatio
		case leaveProgressBars
		case liveStreaming
		case makeSmoothAfterApplyingFunctions
		case markPathsClosed
		case maxAllowableNorm
		case movieFileExtension
		case nChannels
		case nPointsPerCubicCurve
		/// Number of components in the arc
		case numComponents
		case pathArc
		case pathArcAxis
		case pathFunc
		case pixelArrayDataType
		case pngMode
		case preFunctionHandleToAnchorScaleFactor
		/// Radius of an arc
		case radius
		case randomSeed
		case rateFunc
		case remover
		case replaceMObjectsWithTargetsInScene
		case runTime
		case saveLastFrame
		case savePngs
		case sheenDirection
		case sheenFactor
		case skipAnimations
		case startAtAnimationNumber
		case shadeIn3D
		case strokeColour
		case strokeOpacity
		case strokeWidth
		case suspendMObjectUpdating
		case toleranceForPointEquality
		case toTwitch
		case writeToMovie
		case zBuffFunc

		public typealias RawValue = String
	}
}
