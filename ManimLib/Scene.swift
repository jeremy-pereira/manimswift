//
//  Scene.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

internal func notImplemented(_ message: String = "", file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) not implemented, '\(message)'")
}

internal func mustBeOverridden(_ object: AnyObject, file: String = #file, line: Int = #line, function: String = #function) -> Never
{
	fatalError("\(file)(\(line)): \(function) must be overridden in \(type(of: object))")
}

/// Base class for all scenes.
open class Scene: ConfigurableObject
{
	private var mobjects: [MObject] = []
	// TODO: In Manim this is flagged as needing removal
	private var foregroundMObjects: [MObject] = []

	public private(set) var config: Config

	private static let defaultConfig : [Config.Key : Any] =
	[
		.cameraClass : Camera.self,
		.skipAnimations : false,
		.alwaysUpdateMObjects : false,
		.randomSeed : 0,
		.startAtAnimationNumber : Optional<Int>.none as Any,
		.endAtAnimationNumber : Optional<Int>.none as Any,
		.leaveProgressBars : false
	]
	private var numPlays = 0
	private var startAtAnimationNumber: Int?
	private var endAtAnimationNumber: Int?
	private lazy var skipAnimations: Bool = { self.config[.skipAnimations] as! Bool }()
	private(set) var fileWriter: Scene.FileWriter!
	fileprivate var camera: Camera

	public init(config: Config) throws
	{
		let myConfig = config.adding(Scene.defaultConfig)
		// TODO: Use something more dynamic from the config to create the camera
		self.camera = Camera(config: myConfig)
		self.config = myConfig
		try fileWriter = FileWriter(scene: self, config: config)
	}

	open func construct() throws { notImplemented() }

	private func playLikeFunc(theFunc: @escaping ([Animation]) -> ()) -> ([Animation]) throws -> ()
	{
		return {
			animation in
			try self.updateSkippingStatus()
			self.fileWriter.beginAnimation(allowWrite: !self.skipAnimations)
			theFunc(animation)
			self.fileWriter.endAnimation(allowWrite: !self.skipAnimations)
			self.numPlays += 1
		}
	}

	private func updateSkippingStatus() throws
	{
		if let startAtAnimationNumber = startAtAnimationNumber
		{
			skipAnimations = !(numPlays == startAtAnimationNumber)
		}
		if let endAtAnimationNumber = endAtAnimationNumber
		{
			if self.numPlays >= endAtAnimationNumber
			{
				self.skipAnimations = true
				throw Error.endSceneEarly
			}
		}
	}

	public var play: ([Animation]) throws -> ()
	{
		return playLikeFunc
		{
			[weak self] (animations) in
			guard animations.count > 0 else { return }
			guard let strongSelf = self else { return }

			let compiledAnimations = strongSelf.compile(animations: animations)
			strongSelf.begin(animations: compiledAnimations)
			strongSelf.progressThrough(animations: compiledAnimations)
			strongSelf.finish(animations: compiledAnimations)
		}
	}

	// An animation can be either an instance of an animation class or an
	// invocation of an MObject method with arguments. This function will
	// create a flat list of animations
	//
	// TODO: implement the non flat animation bit instead of just returning the
	// list.
	private func compile(animations: [Animation]) -> [Animation]
	{
		return animations
	}

	private func begin(animations: [Animation])
	{
		var currentMObjects = mObjectFamilyMembers
		for animation in animations
		{
			animation.begin()
			self.add(items: [animation.mObject])
			currentMObjects += animation.mObject.family
		}
	}

	private var mObjectFamilyMembers: [MObject]
	{
		return mobjects.flatMap{ $0.family }.removing(isRedundant: { $0 === $1 })
	}

	private func progressThrough(animations: [Animation])
	{
		let movingMObjects = self.getMovingMObjects(animations: animations)
		updateFrame(excluding: movingMObjects)
		let staticImage = self.frame
		var lastT: Double = 0
		for t in self.getTimeProgression(animations: animations)
		{
			let dt = t - lastT
			lastT = t
			for animation in animations
			{
				animation.updateMObjects(deltaTime: dt)
				let alpha = t / animation.runTime
				animation.interpolate(alpha: alpha)
			}
			updateMObjects(deltaTime: dt)
			updateFrame(mobjects: movingMObjects, background: staticImage)
			add(frames: [self.frame])
		}
	}

	private func updateMObjects(deltaTime: Double)
	{
		for mob in mobjects
		{
			mob.update(deltaTime: deltaTime)
		}
	}

	private func getMovingMObjects(animations: [Animation]) -> [MObject]
	{
		// Go through mobjects from start to end, and as soon as there's one
		// that needs updating of some kind per frame, return the list from that
		// point forward.

		let animationMObjects = animations.map { $0.mObject }
		let mObjects = self.mObjectFamilyMembers
		let ret = mObjects.drop
		{
			!animationMObjects.contains($0)
			&& $0.familyUpdaters.isEmpty
			&& !foregroundMObjects.contains($0)
		}
		return Array(ret)
	}

	private func updateFrame(mobjects: [MObject] = [],
							 background: Camera.PixelArray? = nil,
							 includeSubMObjects: Bool = true,
							 ignoreSkipping: Bool = true,
							 excluding: [MObject] = [])
	{
		guard !skipAnimations || ignoreSkipping else { return }
		let mObjectsToUse  = mobjects.isEmpty ? (self.mobjects + self.foregroundMObjects).removing(isRedundant: ===)
											  : mobjects
		if let background = background
		{
			camera.pixelArray = background
		}
		else
		{
			camera.reset()
		}
		camera.capture(mobjects: mObjectsToUse, includeSubMObjects: includeSubMObjects, excluding: excluding)
	}
	private var frame: Camera.PixelArray { return camera.pixelArray }

	private func getTimeProgression(animations: [Animation]) -> ProgressDisplay<Double>
	{
		let runTime = animations.map { $0.runTime }.max() ?? 0

		var timeProgression = getTimeProgression(runTime: runTime)
		let hasEtc = animations.count > 1 ? ", etc." : ""
		timeProgression.prefix = "Animation \(numPlays) \(animations[0])\(hasEtc)"
		return timeProgression
	}

	private func getTimeProgression(runTime: Double, nIterations: Int? = nil, overrideSkip: Bool = false)
		-> ProgressDisplay<Double>
	{
		let times: [Double]

		if skipAnimations && !overrideSkip
		{
			times = [runTime]
		}
		else
		{
			let step = 1 / camera.frameRate
			times = Array(stride(from: 0, through: runTime, by: step))
		}
		return ProgressDisplay(iterable: times, prefix: "", total: nIterations)
	}

	private func add(frames: [Camera.PixelArray]) { notImplemented() }

	private func finish(animations: [Animation]){ notImplemented() }
}

extension Scene: Container
{
	public func add(items: [MObject])
	{
		let tempItems = items + foregroundMObjects
		restructure(mObjectList: &mobjects, toRemove: tempItems)
		self.mobjects += tempItems
	}

	public func remove(items: [MObject])
	{
		restructure(mObjectList: &mobjects, toRemove: items, extractFamiles: false)
	}

	public func restructure(mObjectList: inout [MObject], toRemove: [MObject], extractFamiles: Bool = true)
	{
		let objectsToGo: [MObject]
		if extractFamiles
		{
			objectsToGo = MObject.extractFamilyMembers(mObjects: toRemove)
		}
		else
		{
			objectsToGo = toRemove
		}
		let newList = getRestructured(mObjectList: mObjectList, toRemove: objectsToGo)
		mObjectList = newList
	}

	private func addSafeMObjects(listToExamine: [MObject], setToRemove: Set<MObject>) -> [MObject]
	{
		var newMObjects: [MObject] = []

		for mob in listToExamine
		{
			if !setToRemove.contains(mob)
			{
				let intersect = setToRemove.intersection(mob.family)
				if !intersect.isEmpty
				{
					newMObjects += addSafeMObjects(listToExamine: mob.subMObjects, setToRemove: intersect)
				}
				else
				{
					newMObjects.append(mob)
				}
			}
		}
		return newMObjects
	}


	private func getRestructured(mObjectList: [MObject], toRemove: [MObject]) -> [MObject]
	{
		return addSafeMObjects(listToExamine: mObjectList, setToRemove: Set(toRemove))
	}
}

extension Scene
{
	public enum Error: Swift.Error
	{
		case endSceneEarly
	}
}

extension Scene
{
	/// Something that writes a scene to a file I guess
	///
	/// The file writer will write its output to the directory specified by the
	/// `outputDirectory` config variable. If this is not set, the directory
	/// `videos` is used.
	/// - todo: Pick up `outputDirectory` from config
	struct FileWriter
	{
		fileprivate let scene: Scene

		private let writeToMovie: Bool
		private let liveStreaming: Bool
		private var streamLock = false
		private let fileName: String
		// Root of directory tree for output files
		internal private(set) var outputDirectory: URL = URL(fileURLWithPath: "videos")
		// If we need to save the last frame, this will be where to save it
		private var imageFilePath: URL?
		private var movieFilePath: URL?
		private var gifFilePath: URL?
		private let movieFileExtension: String
		private let saveLastFrame: Bool

		private var partialMovieDirectory: URL!

		private let config: Config

		private static let defaultConfig: [Config.Key : Any] =
		[
			.writeToMovie : false,
			.savePngs : false,
			.pngMode : "RGBA", // TODO: PNG mode should be an enum
			.saveLastFrame : false,
			.movieFileExtension : "mp4",
			.gifFileExtension : "gif",
			.liveStreaming : false,
			.toTwitch : false,
		]

		init(scene: Scene, config: Config) throws
		{
			self.scene = scene
			self.config = config.adding(FileWriter.defaultConfig)
			self.writeToMovie = self.config[.writeToMovie] as! Bool
			self.liveStreaming = self.config[.liveStreaming] as! Bool
			self.movieFileExtension = self.config[.movieFileExtension] as! String
			self.saveLastFrame = self.config[.saveLastFrame] as! Bool
			if let fileName = self.config[.fileName] as? String
			{
				self.fileName = fileName
			}
			else
			{
				self.fileName = "\(type(of: scene))"
			}
			try self.initOutputDirectories()
			self.initAudio()
		}


		private func initAudio()
		{
			// TODO: Initilaise the audio
		}

		mutating private func initOutputDirectories() throws
		{
			let fm = FileManager.default
			let sceneName = self.fileName
			if saveLastFrame
			{
				// Need to create an images directory
				let imageDir = outputDirectory.appendingPathComponent("images")
				try fm.createDirectory(at: imageDir, withIntermediateDirectories: true, attributes: [:])
				self.imageFilePath = imageDir.appendingPathComponent(sceneName).appendingPathExtension(".png")
			}

			if writeToMovie
			{
				let movieDir = outputDirectory.appendingPathComponent(getResolutionDirectoryName())
				try fm.createDirectory(at: movieDir, withIntermediateDirectories: true, attributes: [:])
				self.movieFilePath = movieDir
					.appendingPathComponent(sceneName)
					.appendingPathExtension(movieFileExtension)
				self.gifFilePath = movieDir.appendingPathComponent(sceneName).appendingPathExtension(".gif")
				self.partialMovieDirectory = movieDir
						.appendingPathComponent("partial-movie-files")
						.appendingPathComponent(sceneName)
			}
		}
		/// Begin an animation
		///
		/// - Parameter allowWrite: true if writes are allowed
		mutating func beginAnimation(allowWrite: Bool)
		{
			if writeToMovie && allowWrite
			{
				openMoviePipe()
			}
			if liveStreaming
			{
				streamLock = false
			}
		}

		/// End an animation
		///
		/// - Parameter allowWrite: true if writes are allowed
		func endAnimation(allowWrite: Bool){ notImplemented() }

//		private func getDefaultModuleDirectory() -> String
//		{
//			let inputFileUrl = URL(fileURLWithPath: inputFilePath)
//			return inputFileUrl.deletingPathExtension().lastPathComponent
//		}

		private func getNextPartialMoviePath() -> URL
		{
			let ret = self.partialMovieDirectory
						.appendingPathComponent(String(format: "%05d", scene.numPlays))
						.appendingPathExtension(self.movieFileExtension)
			return ret
		}

		private func openMoviePipe()
		{
			let filePath = getNextPartialMoviePath()
			let tempFilePath = filePath
			notImplemented() // openMoviePipe()
		}

		func getResolutionDirectoryName() -> String
		{
			let pixelHeight = scene.camera.pixelHeight
			let frameRate = scene.camera.frameRate
			return "\(pixelHeight)p\(frameRate)"
		}
	}
}
