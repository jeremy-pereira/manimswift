//
//  Colour.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public enum Colour
{
	case red
	case green
	case blue
	case pink
	case black
	case white

	/// Gives us the RGB value as a vector of three values from 0 to 1
	public var rgb: SIMD3<Double>
	{
		switch self
		{
		case .red:
			return Colour.hexToRGB(hex: 0xff_00_00)
		case .green:
			return Colour.hexToRGB(hex: 0x00_ff_00)
		case .blue:
			return Colour.hexToRGB(hex: 0x00_00_ff)
		case .pink:
			return Colour.hexToRGB(hex: 0xD1_47_BD)
		case .black:
			return SIMD3(repeating: 0)
		case .white:
			return SIMD3(repeating: 1)
		}
	}

	public func rgba(opacity: Double) -> SIMD4<Double>
	{
		precondition(opacity >= 0 && opacity <= 1)

		return SIMD4(x: rgb.x, y: rgb.y, z: rgb.z, w: opacity)
	}

	private static func hexToRGB(hex: UInt32) -> SIMD3<Double>
	{
		let red = Double(hex >> 16 & 0xff)
		let green = Double(hex >> 8 & 0xff)
		let blue = Double(hex & 0xff)
		return SIMD3(x: red / 255, y: green / 255, z: blue / 255)
	}
}
