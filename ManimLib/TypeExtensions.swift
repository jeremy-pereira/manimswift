//
//  TypeExtensions.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 29/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//
import simd

/// A point in a three dimensional space
public typealias Point = SIMD3<Double>

extension BinaryFloatingPoint
{

	/// Tau is 2 * pi.
	///
	///It's more convenient than pi for some calculations
	public static var tau: Self { return 2 * pi }
}


/// Vector describing an axis
public typealias Axis = SIMD3<Double>

public extension Axis
{
	static let up = Axis(x: 0, y: 1, z: 0)
	static let down = Axis(x: 0, y: -1, z: 0)
	static let right = Axis(x: 1, y: 0, z: 0)
	static let left = Axis(x: -1, y: 0, z: 0)
	static let inAxis = Axis(x: 0, y: 0, z: -1)
	static let outAxis = Axis(x: 0, y: 0, z: 1)
	static let x = right
	static let y = up
	static let z = outAxis
	static let upLeft = Axis(x: -1, y: 1, z: 0).unit
	static let upRight = Axis(x: 1, y: 1, z: 0).unit
	static let downLeft = Axis(x: -1, y: -1, z: 0).unit
	static let downRight = Axis(x: 1, y: -1, z: 0).unit
}

public extension Array
{

	/// Stretch an array to the given lenghth.
	///
	/// If count is already >= length, nothing happens. If the fill value is
	/// not specified, the last element of the array is used for the new values.
	/// If the array is empty and no fill value is specified, the program will
	/// abort.
	///
	/// - Parameters:
	///   - length: Lengrth to satretch to
	///   - fillValue: value to the fill new elments with
	mutating func stretch(to length: Int, fillValue: Element? = nil)
	{
		guard length > self.count else { return } // Nothing to do

		let fillElement = fillValue ?? self.last!

		self += Array(repeating: fillElement, count: length - count)
	}

	func removing(isRedundant: (Element, Element) -> Bool) -> [Element]
	{
		var unused = self
		var ret: [Element] = []

		while let thisElement = unused.first
		{
			ret.append(thisElement)
			unused.removeAll { isRedundant(thisElement, $0) }
		}
		return ret
	}
}


// MARK: - Declare conformance to Interpolatable for SIMD3<Double>
extension SIMD3: Interpolatable where Scalar == Double {}

public extension Point
{
	/// The unit vector pointing in the same direction as `self`
	var unit: Point
	{
		return simd_normalize(self)
	}
	/// The length of the vector
	///
	/// I can't believe this property doesn't already exist.
	var length: Double
	{
		return simd_length(self)
	}


	/// The cross product with another vector
	///
	/// - Parameter other: The other vector
	/// - Returns: The cross product
	func cross(_ other: Point) -> Point
	{
		return simd_cross(self, other)
	}


	/// Return the dot product of two points
	///
	/// - Parameter other: The point to make the dotr product with of this point
	/// - Returns: The dor product
	func dot(_ other: Point) -> Double
	{
		return simd_dot(self, other)
	}

	/// The origin
	static let origin = Point(x: 0, y: 0, z: 0)
}


/// A 3x3 matrix suitable for rotations and other transformations.
public struct Matrix
{
	private let simd: simd_double3x3


	/// Iniotialise from an array of matrix rows.
	///
	/// As our matrix is 3 x 3, it is a runtime error if there are not 3 rows
	/// or if each row does not have 3 values in it.
	///
	/// - Parameter rows: Array of rows
	public init(rows: [[Double]])
	{
		guard rows.count == 3 else { fatalError("Invalid row count") }
		let vectors: [simd_double3] = rows.map
		{
			guard $0.count == 3 else { fatalError("Invalid column count") }
			return simd_double3($0)
		}
		self.init(simd: simd_double3x3(rows: vectors))
	}


	/// Initialise from a `sind_double3x3`
	///
	/// This is the fundamental initialiser, since the `Matrix` backing variable
	/// is a `simd_matrix3x3`
	/// - Parameter simd: A `simd_double3x3` from which to initialise
	public init(simd: simd_double3x3)
	{
		self.simd = simd
	}


	/// Return a rotation matrix.
	///
	/// Uses the algorithm described on the Wikipedia page on
	/// [rotation matrices](https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle)
	///
	/// - Parameters:
	///   - angle: The angle through which to rotate.
	///   - axis: The axis around which to rotate. The length of the axis vector
	///           is ignored.
	/// - Returns: A rotation matrix describing the rotation.
	///
	public static func rotationMatrix(angle: Double, axis: Axis) -> Matrix
	{
		let u = axis.unit

		let cosA = cos(angle)
		let sinA = sin(angle)

		let matrixArray: [[Double]] =
		[
			[cosA + u.x * u.x * (1 - cosA)      , u.x * u.y * (1 - cosA) - u.z * sinA, u.x * u.z * (1 - cosA) + u.y * sinA],
			[u.y * u.x * (1 - cosA) + u.z * sinA, cosA + u.y * u.y * (1 - cosA)      , u.y * u.z * (1 - cosA) - u.x * sinA],
			[u.z * u.x * (1 - cosA) - u.y * sinA, u.z * u.y * (1 - cosA) + u.x * sinA, cosA + u.z * u.z * (1 - cosA)      ]
		]
		return Matrix(rows: matrixArray)
	}


	/// Return the transpose of this matrix.
	public var transpose: Matrix
	{
		return Matrix(simd: simd_transpose(simd))
	}

	/// Multiply a vector or point by a matrix
	///
	/// - Parameters:
	///   - lhs: The matrix
	///   - rhs: the vector
	/// - Returns: The result of the multiplication
	public static func *(lhs: Matrix, rhs: Point) -> Point
	{
		return lhs.simd * rhs
	}
}

public extension ClosedRange
{
	/// Use a closed range to constrain a value to its bounds
	///
	/// - Parameter x: The comparable to clip
	/// - Returns: x as long as x is within the range or the lower bound or
	///            upper bound of the range depending on x.
	func clip(_ x: Bound) -> Bound
	{
		if x < lowerBound
		{
			return lowerBound
		}
		else if x > upperBound
		{
			return upperBound
		}
		else
		{
			return x
		}
	}
}
