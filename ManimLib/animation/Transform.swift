//
//  Transform.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 30/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public class Transform: Animation
{
	let targetMObject: MObject?
	let pathArc: Double
	let pathArcAxis: Axis
	let pathFunc: ((Point, Point, Double) -> Point) 
	let replaceMObjectWithTarget: Bool

	private static let defaultConfig: [Config.Key : Any] =
	[
		.pathArc : Double(0),
		.pathArcAxis : Axis.outAxis,
		.replaceMObjectsWithTargetsInScene: false
	]


	public init(_  object: MObject, target: MObject? = nil, config: Config = Config())
	{
		let newConfig = config.adding(Transform.defaultConfig)

		self.targetMObject = target
		self.pathArc = newConfig[.pathArc] as! Double
		self.pathArcAxis = newConfig[.pathArcAxis] as! Axis
		self.replaceMObjectWithTarget = newConfig[.replaceMObjectsWithTargetsInScene] as! Bool
		if let pf = newConfig[.pathFunc] as? ((Point, Point, Double) -> Point)
		{
			self.pathFunc = pf
		}
		else
		{
			if pathArc == 0
			{
				self.pathFunc = Path.straightPath
			}
			else
			{
				self.pathFunc = Path.pathAlongArc(arc: pathArc, axis: pathArcAxis)
			}
		}
		super.init(object, config: newConfig)
	}

	func createTarget() -> MObject
	{
		guard let ret = targetMObject else { fatalError("Need to override createTarget") }
		return ret
	}
}
