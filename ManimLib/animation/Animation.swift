//
//  Animation.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 30/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Base class for all animations
public class Animation: ConfigurableObject
{
	public typealias RateFunc = (Double) -> Double

	private static let defaultRunTime: Double = 1.0
	private static let defaultLagRatio: Double = 0
	private static let defaultConfig: [Config.Key : Any] =
	[
		.runTime : defaultRunTime,
		.rateFunc : { RateFunction.smooth(t: $0) },
		.remover : false,
		.lagRatio : defaultLagRatio,
		.suspendMObjectUpdating : true
	]
	internal let mObject: MObject
	public private(set) var config: Config

	private var startingMObject: MObject?
	private var suspendMObjectUpdating: Bool
	private var rateFunc: RateFunc
	private var lagRatio: Double
	var runTime: Double

	/// Initialise the animation
	///
	/// - todo: Add the rest of the potential parameters.
	/// - Parameter object: Object to animate
	public init(_ object: MObject, config: Config)
	{
		self.config = config.adding(Animation.defaultConfig)
		self.mObject = object
		self.rateFunc = self.config[.rateFunc] as! RateFunc
		self.lagRatio = self.config[.lagRatio] as! Double
		suspendMObjectUpdating = self.config[.suspendMObjectUpdating] as! Bool
		self.runTime = self.config[.runTime] as! Double
	}


	/// Begin the animation.
	///
	///  Do as much initialisation as possible here, rather than during the animation.
	public func begin()
	{
		startingMObject = createStartingMObject()
		if suspendMObjectUpdating
		{
			mObject.suspendUpdating()
		}
		interpolate(alpha: 0)
	}

	func interpolate(alpha: Double)
	{
		let clippedAlpha = (0 ... 1).clip(alpha)
		interpolateMObject(alpha: rateFunc(clippedAlpha))
	}

	private func interpolateMObject(alpha: Double)
	{
		let families = allFamiliesZipped
		for (i, mobs) in families.enumerated()
		{
			let subAlpha = getSubAlpha(alpha: alpha, index: i, numSubMObjects: families.count)
			interpolate(subMObject: mobs.0, startingSumObject: mobs.1, alpha: subAlpha)
		}
	}

	private func getSubAlpha(alpha: Double, index: Int, numSubMObjects: Int) -> Double
	{
		let fullLength = Double(numSubMObjects - 1) * lagRatio + 1
		let value = alpha * fullLength
		let lower = Double(index) * lagRatio
		return (0 ... 1).clip(value - lower)
	}

	func interpolate(subMObject: MObject, startingSumObject: MObject, alpha: Double) { notImplemented() }

	private var allFamiliesZipped: [(MObject, MObject)]
	{
		let sequence1 = self.mObject.familyMembersWithPoints
		guard let sequence2 = self.startingMObject?.familyMembersWithPoints
			else { fatalError("No starting MObject") }
		return Array(zip(sequence1, sequence2))
	}

	private func createStartingMObject() -> MObject
	{
		return mObject.copy()
	}

	func updateMObjects(deltaTime: Double) { notImplemented() }
}
