//
//  ShowCreation.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 30/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// An animation that shows the creation of an object
public class ShowCreation: ShowPartial
{
	private static let defaultConfig: [Config.Key : Any ] =
	[
		.lagRatio : Double(1)
	]

	public override init(_ object: MObject, config: Config = Config())
	{
		super.init(object, config: config.adding(ShowCreation.defaultConfig))
	}
}
