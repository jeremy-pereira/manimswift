//
//  RateFunction.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 16/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// A collection of rate functions
public struct RateFunction
{

	/// A smooth rate function
	///
	/// - Parameters:
	///   - t: Time I think
	///   - inflection: No idea but it defaults to 10
	/// - Returns: A rate or something
	public static func smooth(t: Double, inflection: Double = 10) -> Double
	{
		let error = sigmoid(-inflection / 2)
		return (0 ... 1).clip((sigmoid(inflection * (t - 0.5)) - error) / (1 - 2 * error))
	}


	/// The logistic sigmoid function
	///
	/// - Parameter x: the input value
	/// - Returns: The sigmoid of the input
	public static func sigmoid(_ x: Double) -> Double
	{
		return 1 / (1 + exp(-x))
	}
}
