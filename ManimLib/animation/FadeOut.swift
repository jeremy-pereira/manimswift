//
//  FadeOut.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 30/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public class FadeOut: Transform
{
	private static let defaultLagRatio: Double = 0
	private static let defaultConfig: [Config.Key : Any] =
	[
		.remover : true,
		.lagRatio : defaultLagRatio
	]

	public init(_ object: MObject, config: Config = Config())
	{
		super.init(object, target: nil, config: config.adding(FadeOut.defaultConfig))
	}

	public override func createTarget() -> MObject
	{
		return self.mObject.copy().fade(darkness: 1)
	}
}
