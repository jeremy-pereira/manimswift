//
//  Camera.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 07/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Models a camera
struct Camera: ConfigurableObject
{
	private static let defaultCameraConfig = Config.productionQuality
	private static let defaultFrameHeight: Double = 8
	private static let defaultFrameWidth = defaultFrameHeight * Double(defaultCameraConfig.pixelWidth) / Double(defaultCameraConfig.pixelHeight)


	private static let defaultConfig: [ManimLib.Config.Key : Any] =
	[
		.cameraConfig : defaultCameraConfig,
		.frameHeight : defaultFrameHeight,
		.frameWidth : defaultFrameWidth,
		.frameCentre : Point.origin,
		.backgroundColour: Colour.black,
		.backgroundOpacity : Double(1),
		.maxAllowableNorm : defaultFrameWidth,
		// TODO: Should be an enum
		.imageMode : "RGBA",
		.nChannels : 4,
		// TODO: Should be something other than a string
		.pixelArrayDataType : PixelArray.DataType.uint8,
		.zBuffFunc : { (mobject: MObject) -> Point in mobject.centre },
		.cairoLineWidthMultiple : 0.01
	]
	var config: ManimLib.Config

	// TODO: All implementation of Camera

	let cameraConfig: Config
	let pixelArrayDataType: PixelArray.DataType
	var pixelArray: PixelArray = PixelArray()
	var background: PixelArray
	let backgroundImage: URL?
	let nChannels: Int
	let backgroundColour: Colour
	let backgroundOpacity: Double

	//  extract_mobject_family_members moved to statric function on MObject

	var rgbMaxVal : Int { return pixelArrayDataType.max }
	var pixelHeight: Int { return cameraConfig.pixelHeight }
	var pixelWidth: Int { return cameraConfig.pixelWidth }
	var frameRate: Double { return cameraConfig.frameRate }
	var frameHeight: Double
	var frameWidth: Double

	init(config: ManimLib.Config)
	{
		let newConfig = config.adding(Camera.defaultConfig)
		cameraConfig = newConfig[.cameraConfig] as! Config
		pixelArrayDataType = newConfig[.pixelArrayDataType] as! PixelArray.DataType
		// TODO: init pixel_array_to_cairo_context
		self.config = newConfig
		self.backgroundImage = newConfig[.backgroundImage] as? URL
		self.nChannels = newConfig[.nChannels] as! Int
		background = PixelArray(height: cameraConfig.pixelHeight, width: cameraConfig.pixelWidth, nChannels: nChannels)
		self.backgroundColour = newConfig[.backgroundColour] as! Colour
		self.backgroundOpacity = newConfig[.backgroundOpacity] as! Double
		self.frameHeight = newConfig[.frameHeight] as! Double
		self.frameWidth = newConfig[.frameWidth] as! Double
		initBackground()
		resizeFrameShape()
		reset()
	}

	private mutating func initBackground()
	{
		if let _ = self.backgroundImage
		{
			notImplemented("Getting the background image")
		}
		else
		{
			background.fill(colour: backgroundColour, opacity: backgroundOpacity)
		}
	}

	private mutating func resizeFrameShape(fixHeight: Bool = false)
	{
		let aspectRatio = Double(pixelWidth) / Double(pixelHeight)
		if fixHeight
		{
			frameWidth = aspectRatio * frameHeight
		}
		else
		{
			frameHeight = frameWidth / aspectRatio
		}
	}

	mutating func reset()
	{
		pixelArray = background
	}

	func capture(mobjects: [MObject], includeSubMObjects: Bool, excluding: [MObject] = [])
	{
		let mObjectsToDisplay = getToDisplay(mobjects: mobjects,
											 includeSubMObjects: includeSubMObjects,
											 excluding: excluding)

		let batchedObjects = Dictionary<String, [MObject]>(grouping: mObjectsToDisplay){ $0.typeAsString }


		for typeName in ["VMObject", "PMObject", "AbstractImageMObject", "MObject"]
		{
			if let batch = batchedObjects[typeName]
			{
				switch typeName
				{
				case "VMObject":
					let vmObjects = batch as! [VMObject]
					displayMultiple(vmObjects: vmObjects, pixelArray: pixelArray)
//				case "PMObject":
//					let pmObjects = batch as! [PMObject]
//					displayMultiple(pmObjects: pmObjects, pixelArray: pixelArray)
//				case "AbstractImageMObject":
//					let imageObjects = batch as! [AbstractImageMObject]
//					displayMultiple(imageObjects: imageObjects, pixelArray: pixelArray)
				case "MObject":
					break
				default:
					fatalError("Type \(typeName) shouldn't exist here")
				}
			}
		}
	}

	private func displayMultiple(vmObjects: [VMObject], pixelArray: PixelArray) { notImplemented() }

	private func getToDisplay(mobjects: [MObject],
							  includeSubMObjects: Bool = true,
							  excluding: [MObject] = []) -> [MObject]
	{
		let ret: [MObject]
		if includeSubMObjects
		{
			let allMObjects = self.extractFamilyMembers(mobjects: mobjects, onlyThoseWithPoints: true)
			if !excluding.isEmpty
			{
				let allExcluded = self.extractFamilyMembers(mobjects: mobjects)
				ret = allMObjects.filter{ !allExcluded.contains($0) }
			}
			else
			{
				ret = allMObjects
			}
		}
		else
		{
			ret = mobjects
		}
		return ret
	}

	private func extractFamilyMembers(mobjects: [MObject], onlyThoseWithPoints: Bool = false) -> [MObject]
	{
		let allObjects = mobjects.flatMap { $0.family }.removing(isRedundant: ===)
		return onlyThoseWithPoints ? allObjects.filter({ !$0.points.isEmpty }) : allObjects
	}
}

extension Camera
{
	struct PixelArray
	{
		// Pixels are stored in row major order
		private var pixels: [[SIMD4<Double>]]
		var height: Int { return pixels.count }
		var width: Int { return pixels.isEmpty ? 0 : pixels[0].count }

		init(height: Int, width: Int, nChannels: Int)
		{
			let row = Array<SIMD4<Double>>(repeating: SIMD4<Double>(), count: width)
			pixels = Array<Array<SIMD4<Double>>>(repeating: row, count: height)
		}

		init()
		{
			self.init(height: 1, width: 1, nChannels: 4)
		}

		enum DataType
		{
			case uint8

			var max : Int
			{
				switch self
				{
				case .uint8:
					return Int(UInt8.max)
				}
			}
		}

		subscript(x: Int, y: Int) -> SIMD4<Double>
		{
			get { return pixels[y][x] }
			set { pixels[y][x] = newValue }
		}

		fileprivate mutating func fill(colour: Colour, opacity: Double)
		{
			for y in 0 ..< height
			{
				for x in 0 ..< width
				{
					self[x, y] = colour.rgba(opacity: opacity)
				}
			}
		}
	}
}

extension Camera
{

	/// config for a camera
	///
	/// Defines pixel defimition and frame rate
	public struct Config
	{
		let pixelHeight: Int
		let pixelWidth: Int
		let frameRate: Double

		/// Production quality camera
		public static let productionQuality = Config(pixelHeight: 1440, pixelWidth: 2560, frameRate: 60)
		/// High quality 1080p
		public static let highQuality = Config(pixelHeight: 1080, pixelWidth: 1920, frameRate: 60)
		/// Medium quality 720p
		public static let mediumQuality = Config(pixelHeight: 720, pixelWidth: 1280, frameRate: 30)
		/// Low quality 480p
		public static let lowQuality = Config(pixelHeight: 480, pixelWidth: 854, frameRate: 15)
	}
}
