//
//  Arc.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Describes an Arc of a circle
public class Arc: TipableVMObject
{
	private static let defaultConfig: [Config.Key : Any] =
	[
		.radius : Double(1),
		.numComponents : 9,
		.anchorsSpanFullRange : true,
		.arcCentre : Point.origin
	]

	private let startAngle: Double
	private let angle: Double
	private let radius: Double
	private let arcCentre: Point
	private let numComponents: Int

	public init(startAngle: Double = 0, angle: Double = Double.tau / 4, config: Config)
	{
		self.startAngle = startAngle
		self.angle = angle
		let newConfig = config.adding(Circle.defaultConfig)
		self.radius = newConfig[.radius] as! Double
		self.arcCentre = newConfig[.arcCentre] as! Point
		self.numComponents = newConfig[.numComponents] as! Int
		super.init(config: newConfig)
	}

	required init(other: MObject)
	{
		guard let otherArc = other as? Arc
		else
		{
			fatalError("Cannot copy from a \(type(of: other))")
		}
		self.startAngle = otherArc.startAngle
		self.angle = otherArc.angle
		self.radius = otherArc.radius
		self.arcCentre = otherArc.arcCentre
		self.numComponents = otherArc.numComponents
		super.init(other: other)
	}

	open override func generatePoints()
	{
		setPrepositionedPoints()
		scale(factor: radius, aboutPoint: Point.origin)
		shift(vectors: [arcCentre])
	}

	private func setPrepositionedPoints()
	{
		let dTheta = angle / Double(numComponents - 1)
		let angles = stride(from: startAngle, through: startAngle + angle, by: dTheta)
		let anchors: [Point] = angles.map { cos($0) * Axis.x + sin($0) * Axis.y }
		// Tangents are at 90 degrees to the point vectors we have. Note that
		// this is all 2D at this point.
		let tangentVectors: [Point] = anchors.map { SIMD3<Double>(x: -$0.y, y: $0.x, z: 0) }
		let tangentCount = tangentVectors.count
		let handles1 = zip(anchors, tangentVectors).prefix(tangentCount - 1)
			.map
			{
				(at : (Point, Point)) -> Point in
				let (anchor, tangent) = at
				return anchor + (dTheta / 3) * tangent
			}
		let handles2 = zip(anchors, tangentVectors).suffix(tangentCount - 1)
			.map
			{
				(at : (Point, Point)) -> Point in
				let (anchor, tangent) = at
				return anchor - (dTheta / 3) * tangent
			}
		set(anchors1: Array(anchors.prefix(tangentCount - 1)),
			handles1: Array(handles1),
			handles2: Array(handles2),
			anchors2: Array(anchors.suffix(tangentCount - 1)))
	}
}
