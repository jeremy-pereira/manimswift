//
//  Square.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Models a square
///
public class Square: Rectangle
{
	public init(sideLength: Double = 2.0, config: Config = Config())
	{
		super.init(width: sideLength, height: sideLength, config: config)
	}

	public required init(other: MObject)
	{
		super.init(other: other)
	}
}
