//
//  Polygon.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

public class Polygon: VMObject
{
	public init(vertices: [Point], config: Config)
	{
		super.init(config: config.adding([.colour : Colour.blue]))
		self.setPoints(corners: vertices + [vertices[0]])
	}

	required init(other: MObject)
	{
		super.init(other: other)
	}

	override public func generatePoints() { /* points are set in a different way for polygons */ }
}
