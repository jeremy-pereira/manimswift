//
//  Rectangle.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Models a rectangle.
///
public class Rectangle: Polygon
{
	private static let defaultConfig: [Config.Key : Any] =
	[
		.colour : Colour.white,
		// TODO: These don't strike me as config so much as things that must be true
		.markPathsClosed : true,
		.closeNewPoints : true
	]

	public init(width: Double = 2.0, height: Double = 2.0, config: Config = Config())
	{
		super.init(vertices: [Axis.upLeft, Axis.upRight, Axis.downRight, Axis.downLeft], config: config.adding(Rectangle.defaultConfig))
		// TODO: adjust width and height
	}

	public required init(other: MObject)
	{
		super.init(other: other)
	}


}
