//
//  Circle.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Describes a circle
///

public class Circle: Arc
{
	private static let defaultConfig: [Config.Key : Any] =
	[
		.colour : Colour.red,
		.closeNewPoints : true,
		.anchorsSpanFullRange : false
	]

	public init(config: Config = Config())
	{
		super.init(startAngle: 0, angle: Double.tau, config: config.adding(Circle.defaultConfig))
	}

	public required init(other: MObject)
	{
		super.init(other: other)
	}
}
