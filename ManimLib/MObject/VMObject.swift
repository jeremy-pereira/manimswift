//
//  VMObject.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 29/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

open class VMObject: MObject
{
	private static let defaultConfig: [Config.Key : Any] =
	[
		.fillOpacity : 0.0,
		.strokeOpacity : 1.0,
		.strokeWidth : Config.defaultStrokeWidth,
		.backgroundStrokeColour : Colour.black,
		.backgroundStrokeOpacity : 1.0,
		.backgroundStrokeWidth : Double(0),
		.sheenFactor : 0.0,
		.sheenDirection : Axis.upLeft,
		.closeNewPoints : false,
		.preFunctionHandleToAnchorScaleFactor : 0.01,
		.makeSmoothAfterApplyingFunctions : false,
		.shadeIn3D : false,
		.toleranceForPointEquality : 1e-6,
		.nPointsPerCubicCurve : 4
	]

	private let nPointsPerCubicCurve: Int
	private let fillColour: Colour?
	private let fillOpacity: Double
	private let strokeColour: Colour?
	private let strokeOpacity: Double
	private var strokeWidth: Double
	private let backgroundStrokeColour: Colour
	private let backgroundStrokeOpacity: Double
	private var backgroundStrokeWidth: Double
	private var sheenFactor: Double
	private var sheenDirection: Axis

	private var fillRGBAs: [SIMD4<Double>]
	private var strokeRGBAs: [SIMD4<Double>]
	private var backgroundStrokeRGBAs: [SIMD4<Double>]

	public override init(config: Config)
	{
		let newConfig = config.adding(VMObject.defaultConfig)
		self.nPointsPerCubicCurve = newConfig[.nPointsPerCubicCurve] as! Int
		self.fillColour = newConfig[.fillColour] as? Colour
		self.fillOpacity = newConfig[.fillOpacity] as! Double
		self.strokeColour = newConfig[.strokeColour] as? Colour
		self.strokeOpacity = newConfig[.strokeOpacity] as! Double
		self.strokeWidth = newConfig[.strokeWidth] as! Double
		self.backgroundStrokeColour = newConfig[.backgroundStrokeColour] as! Colour
		self.backgroundStrokeOpacity = newConfig[.backgroundStrokeOpacity] as! Double
		self.backgroundStrokeWidth = newConfig[.backgroundStrokeWidth] as! Double
		self.sheenFactor = newConfig[.sheenFactor] as! Double
		self.sheenDirection = newConfig[.sheenDirection] as! Axis
		self.fillRGBAs = []
		self.strokeRGBAs = []
		self.backgroundStrokeRGBAs = []
		super.init(config: newConfig)
	}

	required public init(other: MObject)
	{
		let otherVMObject = other as! VMObject
		self.nPointsPerCubicCurve = otherVMObject.nPointsPerCubicCurve
		self.fillColour = otherVMObject.fillColour
		self.fillOpacity = otherVMObject.fillOpacity
		self.strokeColour = otherVMObject.strokeColour
		self.strokeOpacity = otherVMObject.strokeOpacity
		self.strokeWidth = otherVMObject.strokeWidth
		self.backgroundStrokeColour = otherVMObject.backgroundStrokeColour
		self.backgroundStrokeOpacity = otherVMObject.backgroundStrokeOpacity
		self.backgroundStrokeWidth = otherVMObject.backgroundStrokeWidth
		self.sheenFactor = otherVMObject.sheenFactor
		self.sheenDirection = otherVMObject.sheenDirection
		self.fillRGBAs = otherVMObject.fillRGBAs
		self.strokeRGBAs = otherVMObject.strokeRGBAs
		self.backgroundStrokeRGBAs = otherVMObject.backgroundStrokeRGBAs

		super.init(other: other)
	}

	/// Gives us a list of the chiuldren objects that are VMObjects
	internal var subVMObjects: [VMObject]
	{
		return super.subMObjects.compactMap{ $0 as? VMObject }
	}
	// This is in `MObject` in Manim but you can't use `setFill` on an `MObject`
	// so for type safety, we put it here for now.
	/// Set the fill colour of this object
	///
	/// - Parameters:
	///   - colour: The colour to set
	///   - opacity: How opaque does it need to be
	///   - family: If true (the default) also sets the colour of any sub objects
	/// - Returns: self, so we can chain `setXXX`s together.
	@discardableResult public func setFill(colour: Colour, opacity: Double = 1, family: Bool = true) -> VMObject
	{
		if family
		{
			subVMObjects.forEach { $0.setFill(colour: colour, opacity: opacity, family: family) }
		}
		updateRGBAs(array: &fillRGBAs, colour: colour, opacity: opacity)
		return self
	}

	/// Set the stroke attributes of this object
	///
	/// - Parameters:
	///   - background: If true sets the background stroke attributes otherwise
	///                 sets the normal stroke attributes (default: `false`)
	///   - colour: The colour to set
	///   - width: Width of the stroke
	///   - opacity: How opaque does it need to be
	///   - family: If true (the default) also sets the colour of any sub objects
	/// - Returns: self, so we can chain `setXXX`s together.

	@discardableResult public func setStroke(background: Bool = false, colour: Colour, width: Double, opacity: Double = 1, family: Bool = true) -> VMObject
	{
		if family
		{
			subVMObjects.forEach { $0.setStroke(colour: colour, width: width, opacity: opacity, family: true) }
		}
		if background
		{
			backgroundStrokeWidth = width
			updateRGBAs(array: &backgroundStrokeRGBAs, colour: colour, opacity: opacity)
		}
		else
		{
			strokeWidth = width
			updateRGBAs(array: &strokeRGBAs, colour: colour, opacity: opacity)
		}

		return self
	}

	@discardableResult public func setSheen(factor: Double, direction: Axis? = nil, family: Bool = true) -> VMObject
	{
		if family
		{
			subVMObjects.forEach { $0.setSheen(factor: factor, direction: direction, family: true) }
		}
		sheenFactor = factor

		if let direction = direction
		{
			sheenDirection = direction
		}

		if factor != 0
		{
			// In manim, the family in both cases is the same as passed in,
			// but I don't think we need to do that because we call this function
			// recursively, anyway.
			setStroke(colour: strokeColour ?? colour, width: strokeWidth, family: false)
			setFill(colour: fillColour ?? colour, family: false)
		}
		return self
	}

	private func generateRGBAs(colours: [Colour], opacities: [Double]) -> [SIMD4<Double>]
	{
		// TODO: Add support for a sheen

		return zip(colours, opacities).map { SIMD4(x: $0.0.rgb.x, y: $0.0.rgb.y, z: $0.0.rgb.z, w: $0.1) }
	}

	private func updateRGBAs(array: inout [SIMD4<Double>], colour: Colour? = nil, opacity: Double? = nil)
	{
		let passedColour = colour ?? .black
		let passedOpacity = opacity ?? 0
		var rgbas = generateRGBAs(colours: [passedColour], opacities: [passedOpacity])
		guard !array.isEmpty
		else
		{
			array = rgbas
			return
		}
		if rgbas.count < array.count
		{
			rgbas.stretch(to: array.count)
		}
		else if array.count < rgbas.count
		{
			array.stretch(to: rgbas.count)
		}
		for i in 0 ..< array.count
		{
			if colour != nil
			{
				array[i].x = rgbas[i].x
				array[i].y = rgbas[i].y
				array[i].z = rgbas[i].z
			}
			if opacity != nil
			{
				array[i].w = rgbas[i].w
			}
		}
	}


	/// Initialises the `VMObject`'s colours and other attributes.
	///
	/// Sets the fill, stroke, background stroke and sheen.
	open override func initColours()
	{
		setFill(colour: fillColour ?? colour, opacity: fillOpacity)
		setStroke(background: false, colour: strokeColour ?? colour, width: strokeWidth, opacity: strokeOpacity)
		setStroke(background: true,
				  colour: backgroundStrokeColour,
				  width: backgroundStrokeWidth,
				  opacity: backgroundStrokeOpacity)
		setSheen(factor: sheenFactor, direction: sheenDirection)

	}

	@discardableResult public func setPoints(corners: [Point]) -> VMObject
	{
		// TODO: implement setPoints(corners: [Point])
		return self
	}

	func set(anchors1: [Point], handles1: [Point], handles2: [Point], anchors2: [Point])
	{
		assert(anchors1.count == anchors2.count
			&& anchors1.count == handles1.count
			&& anchors1.count == handles2.count)

		let totalCount = nPointsPerCubicCurve * anchors1.count
		var points: [Point] = []
		for index in 0 ..< anchors1.count
		{
			points.append(anchors1[index])
			points.append(handles1[index])
			points.append(handles2[index])
			points.append(anchors2[index])
		}
		self.points = points
	}
}


/// Describes shared functionality between a Line and an Arc.
///
/// - todo: This might be better as a protocol
open class TipableVMObject: VMObject
{

}
