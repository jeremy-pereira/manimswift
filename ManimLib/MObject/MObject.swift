//
//  MObject.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 29/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

open class MObject: ConfigurableObject
{
	public private(set) var config: Config

	private var updaters: [MObject]
	private var updatingSuspended = false
	private var dim: Int

	internal private(set) var subMObjects: [MObject]
	/// The points associated with this MObject, if any
	public internal(set) var points: [Point]

	private static let defaultConfig: [Config.Key : Any] =
	[
		.colour : Colour.white,
		.dim : 3,
	]

	public let colour: Colour

	public init(config: Config)
	{
		self.points = []
		self.subMObjects = []
		self.config =  config.adding(MObject.defaultConfig, override: false)
		self.colour = self.config[.colour] as! Colour
		self.dim = self.config[.dim] as! Int
		self.updaters = []
		generatePoints()
		initColours()
	}

	required public init(other: MObject)
	{
		config = other.config
		updatingSuspended = other.updatingSuspended
		subMObjects = other.subMObjects.map{ $0.copy() }
		points = other.points
		colour = other.colour
		dim = other.dim
		updaters = other.updaters
	}

	/// Function to be overridden to generate the points for this object
	open func generatePoints()
	{
		mustBeOverridden(self)
	}

	/// Function that should be overridden to initialise colours.
	open func initColours()
	{
		mustBeOverridden(self)
	}

	/// Flips an object about an axis
	///
	/// It's actually a 180 degree rotation about the axis
	/// - Parameter axis: The axis to rotate about. Default is the y axis
	public func flip(axis: Axis = Axis.y)
	{
		self.rotate(radians: Double.pi, axis: axis)
	}


	/// Rotate around an axis.
	///
	/// - Parameters:
	///   - radians: The angle to rotate
	///   - axis: The axis to rotate about. Default is the z axis
	public func rotate(radians: Double, axis: Axis = Axis.z)
	{
		let rotMatrix = Matrix.rotationMatrix(angle: radians, axis: axis)
		applyToPoints { rotMatrix * $0 }
	}

	func update(deltaTime: Double) { notImplemented() }

	@discardableResult public func fade(darkness: Double, family: Bool = true) -> MObject
	{
		if family
		{
			subMObjects.forEach{ $0.fade(darkness: darkness) }
		}
		return self
	}


	/// Make a copy of this object
	open func copy() -> MObject
	{
		let theClass = type(of: self)
		return theClass.init(other: self)
	}


	/// This `MObject` and all of its sub `MObjects`
	public var family: [MObject]
	{
		let allMObjects = [self] + subMObjects.flatMap{ $0.family }
		return allMObjects.removing(isRedundant: { $0 === $1})
	}

	var familyUpdaters: [MObject]
	{
		return family.flatMap{ $0.updaters }
	}

	func suspendUpdating(recursive: Bool = true)
	{
		updatingSuspended = true
		if recursive
		{
			subMObjects.forEach { $0.suspendUpdating(recursive: true) }
		}
	}

	var familyMembersWithPoints: [MObject]
	{
		return family.filter({ $0.points.count > 0 })
	}

	/// Scale the obkect
	///
	/// Default behaviour is to scale the object about the supplied point.
	///
	/// - Parameters:
	///   - factor: Factor to scale by
	///   - aboutPoint: The point to scale from
	open func scale(factor: Double, aboutPoint: Point)
	{
		applyToPoints(aboutPoint: aboutPoint) { factor * $0 }
	}

	private func applyToPoints(aboutPoint: Point? = nil, aboutEdge: Point = Point.origin, function: (Point) -> Point)
	{
		let centre = aboutPoint ?? getCriticalPoint(direction: aboutEdge)
		for mob in self.familyMembersWithPoints
		{
			mob.points = mob.points.map{ function($0 - centre) + centre }
		}
	}

	private func getCriticalPoint(direction: Point) -> Point
	{
		// Picture a box bounding the mobject. Such a box has 9 'critical
		// points: 4 corners, 4 edge center, the center. This returns one of them.

		guard !allPoints.isEmpty else { return Point.origin }
		var result = Point.origin
		for thisDim in 0 ..< dim
		{
			let pointComponents = allPoints.map{ $0[thisDim] }
			if direction[thisDim] > 0
			{
				result[thisDim] = pointComponents.max()!
			}
			else if direction[thisDim] < 0
			{
				result[thisDim] = pointComponents.min()!
			}
			else
			{
				result[thisDim] = (pointComponents.max()! + pointComponents.min()!) / 2
			}
		}
		return result
	}

	/// All the points in this object and its sub objects
	private var allPoints: [Point]
	{
		return subMObjects.reduce(self.points) { $0 + $1.allPoints }
	}

	/// Shift the object by the sequence of vectors.
	///
	/// - Parameter vectors: The vectors to shift by
	open func shift(vectors: [Point])
	{
		let overallVector = vectors.reduce(Point.origin, +)
		familyMembersWithPoints.forEach
		{
			mob in
			mob.points = mob.points.map{ $0 + overallVector }
		}
	}

	static func extractFamilyMembers(mObjects: [MObject], onlyThoseWithPoints: Bool = false) -> [MObject]
	{
		let method: (MObject) -> [MObject] = onlyThoseWithPoints ? { $0.familyMembersWithPoints } : { $0.family }
		let allObjects = mObjects.flatMap { method($0) }
		return allObjects.removing(isRedundant: ===)
	}

	var centre: Point { notImplemented() }

	/// TRhe dynamic type of self as a string.
	var typeAsString: String { return "\(type(of: self))" }
}

extension MObject: Hashable
{
	public static func == (lhs: MObject, rhs: MObject) -> Bool
	{
		return ObjectIdentifier(lhs) == ObjectIdentifier(rhs)
	}

	public func hash(into hasher: inout Hasher)
	{
		ObjectIdentifier(self).hash(into: &hasher)
	}
}

