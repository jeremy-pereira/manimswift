//
//  Path.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 01/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// A protocol that defines types on whic h we can perform interpolations.
///
/// A type that needs to be interpolatable must be multipliable by a `Double`
/// and must implement addition.
public protocol Interpolatable
{
	static func * (lhs: Self, rhs: Double) -> Self
	static func + (lhs: Self, rhs: Self) -> Self
}

extension Interpolatable
{
	public func interpolate(end: Self, alpha: Double) -> Self
	{
		return self * (1 - alpha) + end * alpha
	}
}

public struct Path
{
	private static let straightPathThreshold = 0.01

	/// A function that returns the points on a straight path
	///
	/// Does a straight forward linear interpolation.
	/// - Parameters:
	///   - start: The starting point
	///   - end: The end point
	///   - alpha: The proportion of the path along the straight line to calculate
	///            the interpolated point
	/// - Returns: The interpolated point
	public static func straightPath(start: Point, end: Point, alpha: Double) -> Point
	{
		return start.interpolate(end: end, alpha: alpha)
	}


	/// Returns a function that calculates interpolations along an arc
	///
	/// - Parameters:
	///   - arc: The arc in radians
	///   - axis: The axis
	/// - Returns: A function that will calculate interpolations along the arc
	public static func pathAlongArc(arc: Double, axis: Axis) -> (Point, Point, Double) -> Point
	{
		// if the angle is really small, pretend the path is straight
		guard abs(arc) > straightPathThreshold else { return straightPath }

		let unitAxis = axis.length == 0 ? Axis.outAxis : (axis / axis.length)
		return {
			(start: Point, end: Point, alpha: Double) -> Point in
			let vector = end - start
			var centre = start + 0.5 * vector
			if arc != Double.pi
			{
				centre += unitAxis.cross(vector / 2) / tan(arc / 2)
			}
			let rotMatrix = Matrix.rotationMatrix(angle: alpha * arc, axis: unitAxis)
			return rotMatrix * (centre + (start - centre))
		}

	}
}

public struct Bezier
{
}
