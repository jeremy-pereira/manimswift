//
//  ProgressDisplay.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 27/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// This provides the needed functionality of `tqdm`.
///
/// The project on which this is based comes from here:
///
/// https://github.com/tqdm/tqdm
public class ProgressDisplay<Element>
{
	/// A prefix string for the progress display
	public var prefix: String?
	public private(set) var iterable: AnySequence<Element>
	public let leave: Bool
	public let total: Int?

	/// Initialise the progress display.
	///
	/// - Parameters:
	///   - iterable: Thing to iterate
	///   - prefix: A prefix string for the progress display, default is no prefix
	///   - total: The expected number of iterations. If `nil` the total is guessed
	///            from the iterable sequence using `underestimateCount`.
	///   - leave: if true, the display is kept after it has finished.
	public init<T : Sequence>(iterable: T, prefix: String? = nil, total: Int?, leave: Bool = true)
		where T.Element == Element
	{
		self.prefix = prefix
		self.leave = leave
		self.total = total
		self.iterable = AnySequence(iterable)
	}
	// TODO: Implement the rest of ProgressDisplay
}

extension ProgressDisplay: Sequence
{
	/// Return an iterator for this display
	///
	/// - Returns: An iterator for this display
	public func makeIterator() -> ProgressDisplay.Iterator
	{
		return Iterator(progressDisplay: self)
	}


	/// A progress display iterator
	public struct Iterator: IteratorProtocol
	{
		private let progressDisplay: ProgressDisplay
		private var iterator: AnyIterator<Element>

		fileprivate init(progressDisplay: ProgressDisplay)
		{
			self.progressDisplay = progressDisplay
			self.iterator = progressDisplay.iterable.makeIterator()
		}

		/// Get the next value in the iterator. This
		///
		/// This returns the next value of the sequence being monitored by the
		/// progress display. Note that it has side effects on the display
		/// itself.
		/// - Returns: The next value in the underlying sequence.
		public mutating func next() -> Element?
		{
			return iterator.next()
		}
	}
}
