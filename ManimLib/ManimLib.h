//
//  ManimLib.h
//  ManimLib
//
//  Created by Jeremy Pereira on 28/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for ManimLib.
FOUNDATION_EXPORT double ManimLibVersionNumber;

//! Project version string for ManimLib.
FOUNDATION_EXPORT const unsigned char ManimLibVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ManimLib/PublicHeader.h>


