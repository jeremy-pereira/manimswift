//
//  Container.swift
//  ManimLib
//
//  Created by Jeremy Pereira on 29/06/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//


/// Something you can add or remove items to I think.
///
public protocol Container
{

	/// Add  a list of `MObject` to the container.
	///
	/// - Parameter items: The objects to add
	func add(items: [MObject])

	/// Remove a list of `MObject` from the container
	///
	/// - Parameter items: The items to remove.
	func remove(items: [MObject])
}

