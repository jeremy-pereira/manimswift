#!/bin/sh

author="Jeremy Pereira"
copyright="Copyright © 2019 Jeremy Pereira"

makeADoc() {

	module=$1
	
	echo "Documenting $module"
	
	readme="$module/README.md"
	
	mkdir -p docs/$module
	jazzy -a "$author" -m $module --readme "$readme" --copyright "$copyright" -o "docs/$module" \
		  -x "-target,$module,-scheme,ManimSwift" --min-acl=internal
}

if [ x"$1" != "x" ] 
then
	makeADoc "$1"
else
	makeADoc ManimLib
fi
