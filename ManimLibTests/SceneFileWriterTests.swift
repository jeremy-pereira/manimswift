//
//  SwfitFileWriterTests.swift
//  ManimLibTests
//
//  Created by Jeremy Pereira on 08/07/2019.
//  Copyright © 2019 Jeremy Pereira. All rights reserved.
//

import XCTest
@testable import ManimLib

class SceneFileWriterTests: XCTestCase
{

    override func setUp()
	{
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown()
	{
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCreateDirectories()
	{
		let fm = FileManager.default
		print("Running in \(fm.currentDirectoryPath)")
		do
		{
			let scene = try Scene(config: Config().adding([.writeToMovie : true]))
			XCTAssert(scene.fileWriter.outputDirectory == URL(fileURLWithPath: "videos"), "output directory is \(scene.fileWriter.outputDirectory)")
			var isDirectory = ObjCBool(false)
			XCTAssert(fm.fileExists(atPath: scene.fileWriter!.outputDirectory.path, isDirectory: &isDirectory), "\(scene.fileWriter.outputDirectory) fails to exist")
			XCTAssert(isDirectory.boolValue, "\(scene.fileWriter.outputDirectory) is not a directory")
		}
		catch
		{
			XCTFail("\(error)")
		}
    }

}
