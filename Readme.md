# ManimSwift
This is an animation engine for maths demos. It's a re-implementation in Swift of [Manim](https://github.com/3b1b/manim). As such, it is licensed under the same MIT licence.

# Design Notes
## Class Hierarchy
We will stick with the same deep class hierarchy that Manim has for the moment. However, there may be opportunities for refactoring. `TipableVMObject` may turn out to be a protocol at some point.
## Attribute Config
Objects in Manim have a system of dictionaries for adding attributes to the object hierarchy. I'm not overlay happy about emulating it.